package streams;


import java.util.*;


public class Main {

	public static void main(String[] args) {
		List<User> users;
		List<User> moreAddresses;
		String nameSurname;
		
		User personDetails1 = new User("pioter", "user", "http://test.org", User.Role.user, "Piotr", "Kowalski", 22);
		User personDetails2 = new User("adrian", "guest", "http://nowa.strona.org", User.Role.guest, "Adrian", "Pawlak", 42);
		User personDetails3 = new User("maryjan", "admin", "http://strona.startowa.pl", User.Role.admin, "Marian", "Lipko", 38);
		User personDetails4 = new User("tadzik", "admin", "http://drugie.podejscie.com.pl", User.Role.admin, "Tadeusz", "Barom", 23);
		User personDetails5 = new User("franek", "user", "http://strona.startowa.pl", User.Role.user, "Franciszek", "Paprota", 54);
		User personDetails6 = new User("antek", "admin", "http://strona.startowa.pl", User.Role.admin, "Antoni", "Marzec", 27);
		personDetails3.addAddress("http://mojadomowa.pl");
		
		users = Arrays.asList(personDetails1, personDetails2, personDetails3, personDetails4, personDetails5, personDetails6);
		
		//findUsersWhoHaveMoreThanOneAddress()
		System.out.println("findUsersWhoHaveMoreThanOneAddress() with stremas and lambda");
		moreAddresses = UserService.findUsersWhoHaveMoreThanOneAddress(users);
		for(User personDetail: moreAddresses) {
			System.out.println(personDetail.getFirstName() + " " + personDetail.getLastName());
		}
		
		//findOldestPerson()
		
		
		//findUserWithLongestUsername()
		
		
		//getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18()
		System.out.println("getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18() with stremas and lambda");
		nameSurname = UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(users);
		System.out.println(nameSurname);
		
		//getSortedPermissionsOfUsersWithNameStartingWithA()
		System.out.println("getSortedPermissionsOfUsersWithNameStartingWithA() with streams and lambda");
		System.out.println(UserService.getSortedPermissionsOfUsersWithNameStartingWithA(users));
		
		//printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS()
		System.out.println("printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS() with streams and lambda");
		UserService.printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(users);
		
		//groupUsersByRole()
		
		//partitionUserByUnderAndOver18()
		
	}

}
