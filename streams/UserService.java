package streams;

import java.util.*;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class UserService {
	
	public static List<User> findUsersWhoHaveMoreThanOneAddress(List<User> users) {
		return users.stream()
		.filter(personDetails -> personDetails.getAddress().size() > 1)
		.collect(Collectors.toList());
	}
	
//	public static Person findOldestPerson(List<User> users) {
//		return users.stream()
//	.sorted((personDetails1, personDetails2) -> personDetails1.getAge() - personDetails2.getAge());
		
//	}
	
//	public static User findUserWithLongestUsername(List<User> users) {
//
//		return users.stream()
//		.map(personDetails -> personDetails.getUserName().length())
//		.
//		;
//		}
	
	public static String getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(List<User> users) {
		return users.stream()
		.filter(personDetails -> personDetails.getAge() > 18)
		.map(personDetails -> personDetails.getFirstName().concat(", ").concat(personDetails.getLastName()))
		.collect(Collectors.joining(", "));
	};
	
	public static List<String> getSortedPermissionsOfUsersWithNameStartingWithA(List<User> users) {
		return users.stream()
		.filter(personDetails -> personDetails.getFirstName().toUpperCase().startsWith("A"))
		.map(personDetails -> personDetails.getPermissionName())
		.sorted()
		.collect(Collectors.toList());
	}
	
	public static void printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(List<User> users) {
		users.stream()
		.filter(personDetails -> personDetails.getLastName().toUpperCase().startsWith("S"))
		.map(personDetails -> personDetails.getPermissionName().toUpperCase())
		.forEach(System.out::println);
	}

//	public static Map<Role, List<User>> groupUsersByRole(List<User> users) {};
	
//	public static Map<Boolean, List<User>> partitionUserByUnderAndOver18(List<User> users) {
//		return null;
//	};
}
